import React, { Component } from 'react';
import './App.scss';
import Cards from './components/Cards/Cards';
import Header from './components/Header/Header';

class App extends Component {
	state = {
		products: [],
		productsInChart: [],
		productsInFavourite: [],
	}

	async componentDidMount() {
		if (localStorage.getItem('products')) {
			const products = JSON.parse(localStorage.getItem('products'));
			this.setState({ products });
			this.filterProductsInCart(products);
			this.filterProductsInFavourites(products);
		}
		else {
			try {
				const productsData = await fetch('./products.json').then(res => res.json());
				const products = productsData.map(product => ({ ...product, isFavourite: false, isInCart: false }));
				this.setState({ products });
				this.filterProductsInCart(products);
				this.filterProductsInFavourites(products);
			} catch (error) {
				console.error(error);
			}
		}

	}

	componentDidUpdate() {
		localStorage.setItem('products', JSON.stringify(this.state.products));
		localStorage.setItem('productsInChart', JSON.stringify(this.state.productsInChart));
		localStorage.setItem('productsInFavourite', JSON.stringify(this.state.productsInFavourite));
	}

	filterProductsInCart = (products) => {
		const productsInChart = products.filter(product => product.isInCart);
		this.setState({ productsInChart });
	}

	filterProductsInFavourites = (products) => {
		const productsInFavourite = products.filter(product => product.isFavourite);
		this.setState({ productsInFavourite });
	}

	toggleProductToFavourites = (productId) => {
		this.setState(({ products }) => {
			const newProducts = [...products];

			const productIndex = newProducts.findIndex(({ id }) => id === productId);
			newProducts[productIndex].isFavourite = !newProducts[productIndex].isFavourite;
			const productsInFavourite = newProducts.filter(product => product.isFavourite);
			return { productsInFavourite, products: newProducts };
		});
	}

	toggleProductToCart = (productId, isInCart) => {
		this.setState(({ products }) => {
			const productIndex = products.findIndex(({ id }) => id === productId);
			products[productIndex].isInCart = !isInCart;
			const productsInChart = products.filter(product => product.isInCart);
			return { productsInChart }
		});
	}

	deleteItemFromFavourites = (productId) => {
		this.setState(({ products }) => {
			const productIndex = products.findIndex(({ id }) => id === productId);
			products[productIndex].isFavourite = false;
			const productsInFavourite = products.filter(product => product.isFavourite);
			return { productsInFavourite };
		});
	}

	deleteItemFromCart = (productId) => {
		this.setState(({ products }) => {
			const productIndex = products.findIndex(({ id }) => id === productId);
			products[productIndex].isInCart = false;
			const productsInChart = products.filter(product => product.isInCart);
			return { productsInChart }
		});
	}

	render() {
		const { products, productsInChart, productsInFavourite } = this.state;

		return (
			<>
				<Header
					productsInChart={productsInChart}
					productsInFavourites={productsInFavourite}
					handleDeleteActions={{ handleFavourites: this.deleteItemFromFavourites, handleCart: this.deleteItemFromCart }} />
				<main className='main'>
					<section className="container section__cards">
						<h2 className='section__title'>Найбільш обговорювані товари</h2>
						<Cards
							cards={products}
							toggleProductToFavourites={this.toggleProductToFavourites}
							toggleProductToCart={this.toggleProductToCart}
						/>
					</section>
				</main>
			</>
		);
	}
}

export default App;