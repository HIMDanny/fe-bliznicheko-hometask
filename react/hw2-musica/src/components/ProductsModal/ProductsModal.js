import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from './ProductsModal.module.scss';
import ProductsModalItem from '../ProductsModalItem/ProductsModalItem';

class ProductsModal extends PureComponent {
	static propTypes = {
		products: PropTypes.arrayOf(PropTypes.object),
	}

	render() {
		const { products, handleDeleteFromList } = this.props;
		return (
			<div className={styles.wrapper}>
				<ul className={styles.list}>
					{products.map(product => (
						<ProductsModalItem key={product.id} product={product} handleDelete={handleDeleteFromList} />
					))}
				</ul>
			</div>
		);
	}
}

export default ProductsModal;
