import React, { PureComponent } from 'react';
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

class Button extends PureComponent {
	static propTypes = {
		backgroundColor: PropTypes.string,
		text: PropTypes.string,
		onClick: PropTypes.func.isRequired,
	}

	static defaultProps = {
		backgroundColor: '',
		text: '',
	}

	render() {
		const { backgroundColor, text, onClick } = this.props;
		return (
			<button
				className={styles.button}
				type='button'
				style={{ backgroundColor }}
				onClick={onClick}
			>
				{text}
			</button>
		);
	}
}

export default Button;
