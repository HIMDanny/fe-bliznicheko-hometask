import React, { PureComponent } from 'react';
import Card from '../Card/Card';
import styles from './Cards.module.scss';

class Cards extends PureComponent {
	render() {
		const { cards, toggleProductToFavourites, toggleProductToCart } = this.props;
		return (
			<ul className={styles.cards}>
				{cards.map(product => (
					<li key={product.id}>
						<Card
							isFavourite={product.isFavourite}
							card={product}
							onAddToFavourites={toggleProductToFavourites}
							onAddToCart={toggleProductToCart}
						/>
					</li>
				))}
			</ul>
		);
	}
}

export default Cards;