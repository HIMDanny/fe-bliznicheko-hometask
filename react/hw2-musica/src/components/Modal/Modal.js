import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import styles from './Modal.module.scss'
import PropTypes from 'prop-types';

const htmlElement = document.documentElement;

class Modal extends PureComponent {
	static propTypes = {
		header: PropTypes.string,
		closeButton: PropTypes.bool,
		text: PropTypes.string,
		actions: PropTypes.node,
		handleOnClose: PropTypes.func.isRequired,
	}

	static defaultProps = {
		header: '',
		closeButton: false,
		text: '',
		actions: <div></div>,
	}

	componentDidMount() {
		htmlElement.classList.add('modal-active');
	}

	componentWillUnmount() {
		htmlElement.classList.remove('modal-active');
	}

	render() {
		const { header, closeButton, text, actions, handleOnClose } = this.props;

		return ReactDOM.createPortal(
			<>
				<div className={styles.modalBackground} onClick={handleOnClose}></div>
				<div className={styles.modal}>
					<header className={styles.modalHeader}>
						<span>{header}</span>
						{closeButton &&
							<button type='button' className={styles.moduleCloseButton} onClick={handleOnClose}>
								<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
									width="50" height="50"
									viewBox="0 0 50 50"
									fill="currentColor">
									<path d="M 7.71875 6.28125 L 6.28125 7.71875 L 23.5625 25 L 6.28125 42.28125 L 7.71875 43.71875 L 25 26.4375 L 42.28125 43.71875 L 43.71875 42.28125 L 26.4375 25 L 43.71875 7.71875 L 42.28125 6.28125 L 25 23.5625 Z"></path>
								</svg>
							</button>}
					</header>
					<div className={styles.modalBody}>
						<p>{text}</p>
					</div>
					<footer>
						{actions}
					</footer>
				</div>
			</>,
			document.getElementById('portal')
		);
	}
}

export default Modal;
