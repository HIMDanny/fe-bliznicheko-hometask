import React, { useEffect } from 'react';
import './App.scss';
import AppRoutes from './AppRoutes';
import { useDispatch } from 'react-redux';
import { getProducts } from './store/products/actionCreators';

const App = () => {

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getProducts());

	}, [dispatch]);

	return (
		<>
			<AppRoutes />
		</>
	);
}

export default App;