import React from 'react';
import { Field, ErrorMessage } from 'formik';
import styles from './CustomField.module.scss';
import PropTypes from 'prop-types';

const CustomField = ({ className, name, label, ...rest }) => {
	return (
		<div className={className}>
			<label htmlFor={name} className={styles.label}>
				{label}
			</label>
			<Field className={styles.field} name={name} id={name} {...rest} />
			<ErrorMessage name={name}>
				{errorMsg => <span className={styles.error}>{errorMsg}</span>}
			</ErrorMessage>
		</div>
	);
};

CustomField.propTypes = {
	className: PropTypes.string,
	name: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
};

CustomField.defaultProps = {
	className: styles.controler,
	name: '',
	label: '',
};

export default CustomField;
