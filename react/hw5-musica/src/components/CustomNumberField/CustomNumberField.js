import React from 'react';
import { ErrorMessage, useField } from 'formik';
import styles from './CustomNumberField.module.scss';
import PropTypes from 'prop-types';
import { PatternFormat } from 'react-number-format';

const CustomField = ({ className, label, ...props }) => {
	const [field] = useField(props);
	return (
		<div className={className}>
			<label htmlFor={field.name} className={styles.label}>
				{label}
			</label>
			<PatternFormat
				format="+38(###)###-##-##"
				allowEmptyFormatting
				mask="#"
				className={styles.field}
				id={field.name}
				{...field}
			/>
			<ErrorMessage name={field.name}>
				{errorMsg => <span className={styles.error}>{errorMsg}</span>}
			</ErrorMessage>
		</div>
	);
};

CustomField.propTypes = {
	className: PropTypes.string,
	name: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
};

CustomField.defaultProps = {
	className: styles.controler,
	name: '',
	label: '',
};

export default CustomField;
