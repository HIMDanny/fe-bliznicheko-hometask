import React from 'react';
import PropTypes from 'prop-types';
import styles from './ProductsModal.module.scss';
import ProductsListItem from '../ProductsListItem/ProductsListItem';

const ProductsModal = ({ products, handleDeleteFromList }) => {
	return (
		<div className={styles.wrapper}>
			<ul className={styles.list}>
				{products.map(product => (
					<ProductsListItem
						key={product.id}
						product={product}
						handleDelete={() => handleDeleteFromList(product.id)}
					/>
				))}
			</ul>
		</div>
	);
};

ProductsModal.propTypes = {
	products: PropTypes.arrayOf(PropTypes.object),
};

export default ProductsModal;
