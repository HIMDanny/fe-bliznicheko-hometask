import React, { useEffect } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import Button from '../../components/Button/Button';
import CheckoutForm from '../../components/CheckoutForm/CheckoutForm';
import Modal from '../../components/Modal/Modal';
import { setModalIsOpen } from '../../store/modal/actionCreators';
import styles from './Cart.module.scss';

const Cart = () => {
	const dispatch = useDispatch();

	const orderInfo = useSelector(store => store.order.orderInfo);

	const productsInCart = useSelector(
		store => store.products.productsInCart,
		shallowEqual
	);
	const isModalOpen = useSelector(store => store.modal.isOpen);
	const modalData = useSelector(store => store.modal.modalData);

	const handleModalCancel = () => {
		dispatch(setModalIsOpen(false));
	};

	useEffect(() => {
		if (orderInfo.total > 0) {
			console.log('Order', orderInfo);
		}
	}, [orderInfo]);

	return (
		<>
			<section className="container">
				<h1 className="section__title">Кошик</h1>
				{!productsInCart.length > 0 ? (
					<p className="section__empty">Товарів не має :(</p>
				) : (
					<div className={styles.wrapper}>
						<div className={styles.formHolder}>
							<CheckoutForm products={productsInCart} />
						</div>
					</div>
				)}
			</section>
			<Modal
				header={modalData.header}
				closeButton
				isOpen={isModalOpen}
				text={modalData.text}
				actions={
					<div>
						<Button text="Скасувати" onClick={handleModalCancel} />
						<Button text={'Видалити'} onClick={modalData.handleSubmit} />
					</div>
				}
			/>
		</>
	);
};

export default Cart;
