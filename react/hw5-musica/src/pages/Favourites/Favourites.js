import React from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import Cards from '../../components/Cards/Cards';

const Favourites = () => {

	const productsInFavourites = useSelector(store => store.products.productsInFavourites, shallowEqual);

	return (
		<section className='container'>
			<h1 className='section__title'>Обрані товари</h1>
			{productsInFavourites.length
				? <Cards products={productsInFavourites} />
				: <p className='section__empty'>Товарів не має :(</p>}
		</section>
	);
}

export default Favourites;
