import produce from 'immer';

import { CREATE_ORDER } from './actions';

const initialValue = {
	orderInfo: {
		total: 0,
		products: [],
		userInfo: {},
	},
};

const orderReducer = (state = initialValue, action) => {
	switch (action.type) {
		case CREATE_ORDER: {
			return produce(state, draftState => {
				draftState.orderInfo = action.payload;
			});
		}
		default:
			return state;
	}
};

export default orderReducer;
