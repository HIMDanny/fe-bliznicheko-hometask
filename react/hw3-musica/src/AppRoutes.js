import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Cart from './pages/Cart/Cart';
import PageLayout from './pages/PageLayout';
import Home from './pages/Home/Home';
import Favourites from './pages/Favourites/Favourites';

const AppRoutes = (
	{
		products,
		toggleProductToFavourites,
		toggleProductToCart,
		productsInCart,
		productsInFavourite,
	}
) => {
	return (
		<Routes>
			<Route path='/' element={
				<PageLayout
					productsInCart={productsInCart}
					productsInFavourite={productsInFavourite}
					toggleProductToFavourites={toggleProductToFavourites}
					toggleProductToCart={toggleProductToCart}
				/>
			}>
				<Route index element={
					<Home
						products={products}
						toggleProductToFavourites={toggleProductToFavourites}
						toggleProductToCart={toggleProductToCart}
					/>
				} />
				<Route path='/cart' element={
					<Cart
						products={productsInCart}
						toggleProductToFavourites={toggleProductToFavourites}
						toggleProductToCart={toggleProductToCart}
					/>
				} />
				<Route path='/favoutires' element={
					<Favourites
						products={productsInFavourite}
						toggleProductToFavourites={toggleProductToFavourites}
						toggleProductToCart={toggleProductToCart}
					/>
				} />
			</Route>
		</Routes>
	);
}

export default AppRoutes;
