import React, { useState, useEffect } from 'react';
import './App.scss';
import AppRoutes from './AppRoutes';

const App = () => {
	const [products, setProducts] = useState([]);
	const [productsInCart, setProductsInCart] = useState([]);
	const [productsInFavourite, setProductsInFavourite] = useState([]);

	const filterProductsInCart = (products) => {
		const productsInCart = products.filter(product => product.isInCart);
		setProductsInCart(productsInCart);
	}

	const filterProductsInFavourites = (products) => {
		const productsInFavourite = products.filter(product => product.isFavourite);
		setProductsInFavourite(productsInFavourite);
	}

	const toggleProductToFavourites = (productId) => {
		const newProducts = [...products];
		const productIndex = newProducts.findIndex(({ id }) => id === productId);
		newProducts[productIndex].isFavourite = !newProducts[productIndex].isFavourite;

		const productsInFavourite = newProducts.filter(product => product.isFavourite);
		setProducts(newProducts);
		setProductsInFavourite(productsInFavourite);
	}

	const toggleProductToCart = (productId) => {
		const newProducts = [...products];
		const productIndex = newProducts.findIndex(({ id }) => id === productId);
		newProducts[productIndex].isInCart = !newProducts[productIndex].isInCart;
		const productsInCart = newProducts.filter(product => product.isInCart);

		setProductsInCart(productsInCart);
	}

	useEffect(() => {
		const getProducts = async () => {
			if (localStorage.getItem('products')) {
				const products = JSON.parse(localStorage.getItem('products'));
				setProducts(products);
				filterProductsInCart(products);
				filterProductsInFavourites(products);
			}
			else {
				try {
					const productsData = await fetch('./products.json').then(res => res.json());
					const products = productsData.map(product => ({ ...product, isFavourite: false, isInCart: false }));
					setProducts(products);
					filterProductsInCart(products);
					filterProductsInFavourites(products);
				} catch (error) {
					console.error(error);
				}
			}
		}

		getProducts();
	}, []);

	useEffect(() => {
		localStorage.setItem('products', JSON.stringify(products));
		localStorage.setItem('productsInCart', JSON.stringify(productsInCart));
		localStorage.setItem('productsInFavourite', JSON.stringify(productsInFavourite));
	}, [products, productsInCart, productsInFavourite]);

	return (
		<>
			<AppRoutes
				products={products}
				toggleProductToFavourites={toggleProductToFavourites}
				toggleProductToCart={toggleProductToCart}
				productsInCart={productsInCart}
				productsInFavourite={productsInFavourite}
			/>
		</>
	);
}

export default App;