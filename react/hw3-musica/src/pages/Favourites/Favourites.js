import React from 'react';
import Cards from '../../components/Cards/Cards';

const Favourites = ({ products, toggleProductToFavourites, toggleProductToCart }) => {
	return (
		<section className='container'>
			<h1 className='section__title'>Обрані товари</h1>
			<Cards
				cards={products}
				toggleProductToFavourites={toggleProductToFavourites}
				toggleProductToCart={toggleProductToCart}
			/>
		</section>
	);
}

export default Favourites;
