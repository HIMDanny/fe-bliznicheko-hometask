import React, { useEffect, useState } from 'react';
import Cards from '../../components/Cards/Cards';
import styles from './Cart.module.scss';

const Cart = ({ products, toggleProductToFavourites, toggleProductToCart }) => {

	const [totalPrice, setTotalPrice] = useState(0);

	useEffect(() => {
		const calculateTotalPrice = () => {
			if (products.length) {
				return products.reduce((accum, current) => accum + current.price, 0);
			}

			return 0;
		}

		setTotalPrice(calculateTotalPrice);

	}, [products]);

	return (
		<>
			<section className='container'>
				<h1 className='section__title'>Товари у кошику</h1>
				<Cards
					cards={products}
					toggleProductToFavourites={toggleProductToFavourites}
					toggleProductToCart={toggleProductToCart}
				/>
			</section>
			<div className={styles.footer}>
				<div className={styles.footerInner}>
					<span className={styles.total}>
						<span>Всього: </span>
						{totalPrice}$
					</span>
				</div>
			</div>
		</>
	);
}

export default Cart;
