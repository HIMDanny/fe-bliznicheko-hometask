import React, { useState, useEffect, useRef } from 'react';
import styles from './Cart.module.scss';
import PropTypes from 'prop-types';
import ProductsModal from '../ProductsModal/ProductsModal';
import { ReactComponent as CartIcon } from '../../svg/cart-icon.svg';

const Cart = ({ productsInCart, handleDeleteFromList }) => {
	const [isInfoOpen, setIsInfoOpen] = useState(false);

	const infoModal = useRef(null);
	const favouritesBtn = useRef(null);

	useEffect(() => {
		const checkIfClickedOutside = e => {
			// If the menu is open and the clicked target is not within the menu,
			// then close the menu
			if (!favouritesBtn.current.contains(e.target) && isInfoOpen && infoModal.current && !infoModal.current.contains(e.target)) {
				setIsInfoOpen(false);
			}
		}

		document.addEventListener('mousedown', checkIfClickedOutside);

		if (!productsInCart.length) setIsInfoOpen(false);

		return () => document.removeEventListener('mousedown', checkIfClickedOutside);
	}, [isInfoOpen, productsInCart.length]);

	return (
		<div className={styles.holder}>
			<button
				ref={favouritesBtn}
				type='button'
				className={styles.cartButton}
				data-products-in-cart={productsInCart.length}
				onClick={() => {
					if (productsInCart.length) setIsInfoOpen(prev => !prev);
				}}>
				<CartIcon />
			</button>
			{isInfoOpen &&
				<div ref={infoModal} className={styles.infoModal}>
					<ProductsModal products={productsInCart} handleDeleteFromList={handleDeleteFromList} />
				</div>}
		</div>
	);
}

Cart.propTypes = {
	productsInCart: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default Cart;
