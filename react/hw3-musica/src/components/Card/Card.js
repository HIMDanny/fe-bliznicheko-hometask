import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './Card.module.scss';
import Modal from '../Modal/Modal';
import Button from '../Button/Button';
import { ReactComponent as FavouriteIcon } from '../../svg/favourite-icon.svg';

const Card = ({ card: { title, thumbnail, price, color, id, isInCart }, isFavourite, onAddToCart, onAddToFavourites }) => {
	const [hasRequestedToAddToCart, setHasRequestedToAddToCart] = useState(false);

	const closeModal = () => {
		setHasRequestedToAddToCart(false);
	}

	return (
		<>
			<article className={styles.card}>
				<button type='button' className={styles.addToFavouriteButton} onClick={() => {
					onAddToFavourites(id);
				}}>
					<FavouriteIcon data-is-favourite={isFavourite} />
				</button>
				<div className={styles.imgContainer}>
					<img src={thumbnail} alt={title} />
				</div>
				<div className={styles.mainContent}>
					<h6 className={styles.title}>{title}</h6>
					<span className={styles.color}><span>Колір:</span> {color}</span>
					<span className={styles.id}>Артикул: {id}</span>
				</div>
				<footer className={styles.footer}>
					<span className={styles.price}>{price}</span>
					<button
						className={styles.addToCartButton}
						onClick={() => setHasRequestedToAddToCart(true)}>
						{isInCart ? 'Видалити з кошика' : 'Додати до кошика'}
					</button>
				</footer>
			</article >

			<Modal
				header={!isInCart ? 'Додати товар до кошика?' : 'Видалити товар з кошика?'}
				closeButton
				handleOnClose={closeModal}
				isOpen={hasRequestedToAddToCart}
				actions={
					<div>
						<Button text='Скасувати' onClick={() => {
							closeModal();
						}} />
						<Button text={!isInCart ? 'Додати' : 'Видалити'} onClick={() => {
							onAddToCart(id);
							closeModal();
						}} />
					</div>
				}
			/>

		</>
	);
}

Card.propTypes = {
	card: PropTypes.shape({
		id: PropTypes.number.isRequired,
		title: PropTypes.string,
		price: PropTypes.number,
		thumbnail: PropTypes.string,
		color: PropTypes.string,
		isFavourite: PropTypes.bool,
		isInCart: PropTypes.bool,
	}),
	onAddToCart: PropTypes.func.isRequired,
	onAddToFavourites: PropTypes.func.isRequired,
}

Card.defaultProps = {
	card: {
		id: 0,
		title: '',
		price: 0,
		thumbnail: '',
		color: '',
		isFavourite: false,
		isInCart: false,
	},
}

export default Card;

