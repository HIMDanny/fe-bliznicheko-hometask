import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import styles from './Modal.module.scss'
import PropTypes from 'prop-types';
import { ReactComponent as CloseIcon } from '../../svg/close-icon.svg';

const htmlElement = document.documentElement;

const Modal = ({ header, closeButton, text, actions, handleOnClose, isOpen }) => {

	useEffect(() => {
		isOpen && htmlElement.classList.add('modal-active');
		return () => htmlElement.classList.remove('modal-active');
	});

	if (!isOpen) return null;
	else {
		return ReactDOM.createPortal(
			<>
				<div className={styles.modalBackground} onClick={handleOnClose}></div>
				<div className={styles.modal}>
					<header className={styles.modalHeader}>
						<span>{header}</span>
						{closeButton &&
							<button type='button' className={styles.moduleCloseButton} onClick={handleOnClose}>
								<CloseIcon />
							</button>}
					</header>
					<div className={styles.modalBody}>
						<p>{text}</p>
					</div>
					<footer>
						{actions}
					</footer>
				</div>
			</>,
			document.getElementById('portal')
		);
	}
}

Modal.propTypes = {
	header: PropTypes.string,
	closeButton: PropTypes.bool,
	text: PropTypes.string,
	actions: PropTypes.node,
	handleOnClose: PropTypes.func.isRequired,
	isOpen: PropTypes.bool.isRequired,
}

Modal.defaultProps = {
	header: '',
	closeButton: false,
	text: '',
	actions: <div></div>,
}

export default Modal;
