import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Cart from './pages/Cart/Cart';
import PageLayout from './pages/PageLayout';
import Home from './pages/Home/Home';
import Favourites from './pages/Favourites/Favourites';

const AppRoutes = () => {
	return (
		<Routes>
			<Route path='/' element={
				<PageLayout />
			}>
				<Route index element={
					<Home />
				} />
				<Route path='/cart' element={
					<Cart />
				} />
				<Route path='/favoutires' element={
					<Favourites />
				} />
			</Route>
		</Routes>
	);
}

export default AppRoutes;
