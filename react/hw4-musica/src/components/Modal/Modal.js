import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import styles from './Modal.module.scss'
import PropTypes from 'prop-types';
import { ReactComponent as CloseIcon } from '../../svg/close-icon.svg';
import { useDispatch } from 'react-redux';
import { setModalIsOpen } from '../../store/modal/actionCreators';

const htmlElement = document.documentElement;

const Modal = ({ header, closeButton, text, actions, isOpen }) => {

	const dispatch = useDispatch();

	const handleCloseModal = () => dispatch(setModalIsOpen(false));

	useEffect(() => {
		isOpen && htmlElement.classList.add('modal-active');
		return () => htmlElement.classList.remove('modal-active');
	});

	if (!isOpen) return null;
	else {
		return ReactDOM.createPortal(
			<>
				<div className={styles.modalBackground} onClick={handleCloseModal}></div>
				<div className={styles.modal}>
					<header className={styles.modalHeader}>
						<span>{header}</span>
						{closeButton &&
							<button type='button' className={styles.moduleCloseButton} onClick={handleCloseModal}>
								<CloseIcon />
							</button>}
					</header>
					<div className={styles.modalBody}>
						<p>{text}</p>
					</div>
					<footer>
						{actions}
					</footer>
				</div>
			</>,
			document.getElementById('portal')
		);
	}
}

Modal.propTypes = {
	header: PropTypes.string,
	closeButton: PropTypes.bool,
	text: PropTypes.string,
	actions: PropTypes.node,
	isOpen: PropTypes.bool.isRequired,
}

Modal.defaultProps = {
	header: '',
	closeButton: false,
	text: '',
	actions: <div></div>,
}

export default Modal;
