import React from 'react';
import styles from './ProductsModalItem.module.scss';
import PropTypes from 'prop-types';
import { ReactComponent as CloseIcon } from '../../svg/close-icon.svg';

const ProductsModalItem = ({ product: { id, title, price, thumbnail }, handleDelete }) => {
	return (
		<li className={styles.item}>
			<img className={styles.img} src={thumbnail} alt={title} />
			<span className={styles.title}>{title}</span>
			<span className={styles.price}>{price}</span>
			<button type='button' className={styles.deleteButton} onClick={() => handleDelete(id)}>
				<CloseIcon />
			</button>
		</li>
	);
}

ProductsModalItem.propTypes = {
	product: PropTypes.shape({
		id: PropTypes.number.isRequired,
		title: PropTypes.string,
		price: PropTypes.number,
		thumbnail: PropTypes.string,
		color: PropTypes.string,
		isFavourite: PropTypes.bool,
		isInCart: PropTypes.bool,
	}).isRequired,
	handleDelete: PropTypes.func.isRequired,
}

export default ProductsModalItem;
