import { combineReducers } from "redux";
import productsReducer from "./products/productsReducer";
import modalReducer from "./modal/modalReducer";

const rootReducer = combineReducers({
	products: productsReducer,
	modal: modalReducer,
});

export default rootReducer;