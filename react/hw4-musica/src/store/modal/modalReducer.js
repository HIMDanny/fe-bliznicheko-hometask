import produce from "immer";

import {
	SET_MODAL_IS_OPEN,
	SET_MODAL_DATA,
} from "./actions";

const initialValue = {
	isOpen: false,
	modalData: {
		isProductInCart: false,
		header: '',
		text: '',
		handleSubmit: () => { },
	},
}

const modalReducer = (state = initialValue, action) => {
	switch (action.type) {
		case SET_MODAL_IS_OPEN: {
			return produce(state, draftState => {
				draftState.isOpen = action.payload;
			});
		}
		case SET_MODAL_DATA: {
			return produce(state, draftState => {
				draftState.modalData = action.payload;
			});
		}
		default:
			return state;
	}
}

export default modalReducer;