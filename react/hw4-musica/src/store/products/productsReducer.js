import produce from "immer";

import {
	GET_PRODUCTS,
	TOGGLE_PRODUCT_IN_CART,
	TOGGLE_PRODUCT_IN_FAVOURITES,
} from "./actions";

const initialValue = {
	products: [],
	productsInCart: [],
	productsInFavourites: [],
}

const productsReducer = (state = initialValue, action) => {
	switch (action.type) {
		case GET_PRODUCTS: {
			return produce(state, draftState => {
				const productsInCart = action.payload.filter(product => product.isInCart);
				const productsInFavourites = action.payload.filter(product => product.isFavourite);

				draftState.products = action.payload;
				draftState.productsInCart = productsInCart;
				draftState.productsInFavourites = productsInFavourites;

				localStorage.setItem('products', JSON.stringify(draftState.products));
				localStorage.setItem('productsInCart', JSON.stringify(draftState.productsInCart));
				localStorage.setItem('productsInFavourites', JSON.stringify(draftState.productsInFavourites));
			});
		}
		case TOGGLE_PRODUCT_IN_CART: {
			return produce(state, draftState => {
				const index = draftState.products.findIndex(({ id }) => id === action.payload);
				draftState.products[index].isInCart = !draftState.products[index].isInCart;
				draftState.productsInCart = draftState.products.filter(({ isInCart }) => isInCart);

				localStorage.setItem('products', JSON.stringify(draftState.products));
				localStorage.setItem('productsInCart', JSON.stringify(draftState.productsInCart));
			});
		}
		case TOGGLE_PRODUCT_IN_FAVOURITES: {
			return produce(state, draftState => {
				const index = draftState.products.findIndex(({ id }) => id === action.payload);
				draftState.products[index].isFavourite = !draftState.products[index].isFavourite;
				draftState.productsInFavourites = draftState.products.filter(({ isFavourite }) => isFavourite);

				localStorage.setItem('products', JSON.stringify(draftState.products));
				localStorage.setItem('productsInFavourites', JSON.stringify(draftState.productsInFavourites));
			});
		}
		default:
			return state;
	}
}

export default productsReducer;