import {
	GET_PRODUCTS,
	TOGGLE_PRODUCT_IN_CART,
	TOGGLE_PRODUCT_IN_FAVOURITES,
} from "./actions";

export const getProducts = () => async dispatch => {
	const productsFromLS = localStorage.getItem('products');

	if (productsFromLS) {
		const products = JSON.parse(productsFromLS);
		dispatch({ type: GET_PRODUCTS, payload: products });
	}
	else {
		try {
			const productsData = await fetch('./products.json').then(res => res.json());
			const products = productsData.map(product => ({ ...product, isFavourite: false, isInCart: false }));

			dispatch({ type: GET_PRODUCTS, payload: products });
		} catch (error) {
			console.error(error);
		}
	}
}

export const toggleProductInCart = id => ({ type: TOGGLE_PRODUCT_IN_CART, payload: id });
export const toggleProductInFavourites = id => ({ type: TOGGLE_PRODUCT_IN_FAVOURITES, payload: id });