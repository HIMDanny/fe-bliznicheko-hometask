import React, { useEffect, useState } from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import Cards from '../../components/Cards/Cards';
import styles from './Cart.module.scss';

const Cart = () => {

	const [totalPrice, setTotalPrice] = useState(0);

	const productsInCart = useSelector(store => store.products.productsInCart, shallowEqual);

	useEffect(() => {
		const calculateTotalPrice = () => {
			if (productsInCart.length) {
				return productsInCart.reduce((accum, current) => accum + current.price, 0);
			}

			return 0;
		}

		setTotalPrice(calculateTotalPrice);

	}, [productsInCart]);

	return (
		<>
			<section className='container'>
				<h1 className='section__title'>Товари у кошику</h1>
				{productsInCart.length
					? <Cards products={productsInCart} />
					: <p className='section__empty'>Товарів не має :(</p>}
			</section>
			<div className={styles.footer}>
				<div className={styles.footerInner}>
					<span className={styles.total}>
						<span>Всього: </span>
						{totalPrice}$
					</span>
				</div>
			</div>
		</>
	);
}

export default Cart;
