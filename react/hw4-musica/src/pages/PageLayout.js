import React from 'react';
import Header from '../components/Header/Header';
import '../App.scss';
import { Outlet } from 'react-router-dom';

const PageLayout = () => {
	return (
		<>
			<Header />
			<main className='main'>
				<Outlet />
			</main>
		</>
	);
}

export default PageLayout;
