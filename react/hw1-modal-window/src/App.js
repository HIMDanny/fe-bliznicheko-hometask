import React, { Component } from 'react';
import styles from './App.module.scss';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';

class App extends Component {
	state = {
		activeModal: null,
	}

	openModal = (modalToOpen) => this.setState({ activeModal: modalToOpen });
	closeModal = () => this.setState({ activeModal: null });

	render() {
		const { activeModal } = this.state;
		return (
			<div className={styles.wrapper}>
				<div className={styles.buttonContainer}>
					<Button text="Open first modal" backgroundColor="rgb(119, 77, 77)" onClick={() => this.openModal(1)} />
					<Button text="Open second modal" backgroundColor="rgb(55, 93, 163)" onClick={() => this.openModal(2)} />
				</div>
				{activeModal === 1 && <Modal
					header='Do you want to delete this file?'
					text='Once you delete this file, it won’t be possible to undo this action.
				Are you sure you want to delete it?'
					handleOnClose={this.closeModal}
					closeButton
					actions={
						<div>
							<Button text='Ok' backgroundColor="rgb(0, 0, 0, 0.3)" onClick={this.closeModal} />
							<Button text='Cancel' backgroundColor="rgb(0, 0, 0, 0.3)" onClick={this.closeModal} />
						</div>
					}
				/>}
				{activeModal === 2 && <Modal
					header='Another example of Modal'
					text='Hello, opener!'
					handleOnClose={this.closeModal}
					actions={
						<div>
							<Button text='Alert' backgroundColor="rgb(255, 249, 38, 0.4)" onClick={() => alert('HEY!')} />
							<Button text='Log' backgroundColor="rgb(38, 118, 255, 0.4)" onClick={() => { console.log('Another HEY!') }} />
						</div>
					}
				/>}
			</div>
		);
	}
}

export default App;
