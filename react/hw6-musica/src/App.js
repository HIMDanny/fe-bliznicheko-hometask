import React, { useEffect } from 'react';
import './App.scss';
import AppRoutes from './AppRoutes';
import { useDispatch } from 'react-redux';
import { getProducts } from './store/products/actionCreators';
import ViewSwitcherContextProvider from './contexts/ViewSwitcherContext/ViewSwitcherContextProvider';

const App = () => {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getProducts());
	}, [dispatch]);

	return (
		<>
			<ViewSwitcherContextProvider>
				<AppRoutes />
			</ViewSwitcherContextProvider>
		</>
	);
};

export default App;
