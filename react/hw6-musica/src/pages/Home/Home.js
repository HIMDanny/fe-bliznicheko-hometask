import React from 'react';
import Cards from '../../components/Cards/Cards';
import { useSelector } from 'react-redux';
import ProductsViewSwitcher from '../../components/ProductsViewSwitcher/ProductsViewSwitcher';

const Home = () => {
	const products = useSelector(store => store.products.products);

	return (
		<section className="container">
			<header className="section__header">
				<h2 className="section__title">Найбільш обговорювані товари</h2>
				<ProductsViewSwitcher />
			</header>
			<div>
				<Cards products={products} />
			</div>
		</section>
	);
};

export default Home;
