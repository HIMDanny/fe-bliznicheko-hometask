import { createContext } from 'react';

const ViewSwitcherContext = createContext();

export default ViewSwitcherContext;
