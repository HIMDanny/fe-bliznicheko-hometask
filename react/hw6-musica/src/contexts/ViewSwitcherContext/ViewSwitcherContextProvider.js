import { useState } from 'react';
import ViewSwitcherContext from './ViewSwitcherContext';

const ViewSwitcherContextProvider = ({ children }) => {
	const [currentView, setCurrentView] = useState({
		isCards: true,
		isTable: false,
	});

	const setView = view => {
		switch (view) {
			case 'cards': {
				setCurrentView({ isCards: true, isTable: false });
				break;
			}

			case 'table': {
				setCurrentView({ isCards: false, isTable: true });
				break;
			}

			default:
				throw new Error();
		}
	};

	return (
		<ViewSwitcherContext.Provider value={{ currentView, setView }}>
			{children}
		</ViewSwitcherContext.Provider>
	);
};

export default ViewSwitcherContextProvider;
