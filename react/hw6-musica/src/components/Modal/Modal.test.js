import Modal from './ModalWithoutPortal';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import { Provider } from 'react-redux';
import store from '../../store';
import React, { useState } from 'react';

const Component = () => {
	const [isOpen, setIsOpen] = useState(false);

	return (
		<Provider store={store}>
			<button onClick={() => setIsOpen(prev => !prev)}>Change state</button>
			<Modal
				header="header"
				closeButton
				isOpen={isOpen}
				text="test"
				actions={
					<div>
						<button>text</button>
					</div>
				}
			/>
		</Provider>
	);
};

describe('Test Modal states', () => {
	test('should modal be closed', () => {
		render(<Component />);

		expect(screen.queryByTestId('modal-background')).not.toBeInTheDocument();
	});

	test('should modal open', () => {
		render(<Component />);

		const btn = screen.getByText('Change state');
		fireEvent.click(btn);

		const bg = screen.getByTestId('modal-background');
		expect(bg).toBeInTheDocument();
	});

	test('should modal close', () => {
		render(<Component />);

		const btn = screen.getByText('Change state');
		fireEvent.click(btn);

		const bg = screen.getByTestId('modal-background');

		fireEvent.click(btn);
		expect(bg).not.toBeInTheDocument();
	});
});

describe('Modal snapshot testing', () => {
	test('should Modal not to render without isOpen props', () => {
		const { asFragment } = render(
			<Provider store={store}>
				<Modal isOpen={false} />
			</Provider>
		);
		expect(asFragment()).toMatchSnapshot();
	});

	test('should Modal render with isOpen props', () => {
		const { asFragment } = render(
			<Provider store={store}>
				<Modal isOpen={true} text="Some title" />
			</Provider>
		);
		expect(asFragment()).toMatchSnapshot();
	});
});
