import Button from './Button';
import { render, screen, fireEvent } from '@testing-library/react';

const handleClick = jest.fn();

describe('Button handle click', () => {
	test('should onClick work', () => {
		render(<Button onClick={handleClick} text={'Button'} />);

		const btn = screen.getByText('Button');
		fireEvent.click(btn);

		expect(handleClick).toHaveBeenCalled();
	});
});
