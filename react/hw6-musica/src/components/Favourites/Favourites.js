import React, { useState, useEffect, useRef } from 'react';
import styles from './Favourites.module.scss';
import ProductsModal from '../ProductsModal/ProductsModal';
import { ReactComponent as FavouriteIcon } from '../../svg/favourite-icon.svg';
import { useDispatch, useSelector } from 'react-redux';
import { toggleProductInFavourites } from '../../store/products/actionCreators';

const Favourites = () => {
	const [isInfoOpen, setIsInfoOpen] = useState(false);

	const dispatch = useDispatch();
	const productsInFavourites = useSelector(store => store.products.productsInFavourites);

	const handleDeleteFromFavourites = (id) => {
		dispatch(toggleProductInFavourites(id));
	}

	const infoModal = useRef(null);
	const favouritesBtn = useRef(null);

	useEffect(() => {
		const checkIfClickedOutside = e => {
			// If the menu is open and the clicked target is not within the menu,
			// then close the menu
			if (!favouritesBtn.current.contains(e.target) && isInfoOpen && infoModal.current && !infoModal.current.contains(e.target)) {
				setIsInfoOpen(false);
			}
		}

		document.addEventListener('mousedown', checkIfClickedOutside);

		if (!productsInFavourites.length) setIsInfoOpen(false);

		return () => document.removeEventListener('mousedown', checkIfClickedOutside);
	}, [isInfoOpen, productsInFavourites.length]);


	return (
		<div className={styles.holder}>
			<button
				ref={favouritesBtn}
				type='button'
				className={styles.favouritesButton}
				data-products-in-favourites={productsInFavourites.length}
				onClick={() => {
					if (productsInFavourites.length) setIsInfoOpen(prev => !prev);
				}}
			>
				<FavouriteIcon />
			</button>
			{isInfoOpen &&
				<div ref={infoModal} className={styles.infoModal}>
					<ProductsModal products={productsInFavourites} handleDeleteFromList={handleDeleteFromFavourites} />
				</div>}
		</div>
	);
}

export default Favourites;