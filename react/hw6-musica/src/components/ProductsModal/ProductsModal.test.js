import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import ProductsModal from './ProductsModal';

const products = [{ id: 1, title: 'product', price: 2000 }];

describe('ProductsModal snapshot testing', () => {
	test('should ProductsModal match snapshot', () => {
		const { asFragment } = render(<ProductsModal products={products} />);

		expect(asFragment()).toMatchSnapshot();
	});
});

describe('ProductsModal render products testing', () => {
	test('should render product title', () => {
		render(<ProductsModal products={products} />);

		const title = screen.getByText('product');

		expect(title).toBeInTheDocument();
	});
});
