import React from 'react';
import { NavLink } from 'react-router-dom';
import Cart from '../Cart/Cart';
import Favourites from '../Favourites/Favourites';
import styles from './Header.module.scss';

const Header = () => {
	return (
		<header className={styles.header}>
			<div className={styles.inner}>
				<span className={styles.title}>GG</span>
				<ul className={styles.pages}>
					<li>
						<NavLink
							end
							className={({ isActive }) => (isActive ? styles.current : '')}
							to="/"
						>
							Головна
						</NavLink>
					</li>
					<li>
						<NavLink
							className={({ isActive }) => (isActive ? styles.current : '')}
							to="/cart"
						>
							Кошик
						</NavLink>
					</li>
					<li>
						<NavLink
							className={({ isActive }) => (isActive ? styles.current : '')}
							to="/favoutires"
						>
							Обране
						</NavLink>
					</li>
				</ul>
				<div className={styles.counters}>
					<Favourites />
					<Cart />
				</div>
			</div>
		</header>
	);
};

export default Header;
