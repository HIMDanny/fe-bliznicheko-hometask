import React, { useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ViewSwitcherContext from '../../contexts/ViewSwitcherContext/ViewSwitcherContext';
import { setModalIsOpen } from '../../store/modal/actionCreators';
import Button from '../Button/Button';
import Card from '../Card/Card';
import Modal from '../Modal/Modal';
import styles from './Cards.module.scss';

const Cards = ({ products }) => {
	const dispatch = useDispatch();

	const isModalOpen = useSelector(store => store.modal.isOpen);
	const modalData = useSelector(store => store.modal.modalData);

	const { currentView } = useContext(ViewSwitcherContext);

	const handleModalCancel = () => {
		dispatch(setModalIsOpen(false));
	};

	return (
		<>
			<ul
				className={`${styles.cards} ${
					currentView.isCards ? styles.cardView : styles.tableView
				}`}
			>
				{products.map(product => (
					<li key={product.id}>
						<Card card={product} />
					</li>
				))}
			</ul>

			<Modal
				header={modalData.header}
				closeButton
				isOpen={isModalOpen}
				text={modalData.text}
				actions={
					<div>
						<Button text="Скасувати" onClick={handleModalCancel} />
						<Button
							text={!modalData.isProductInCart ? 'Додати' : 'Видалити'}
							onClick={modalData.handleSubmit}
						/>
					</div>
				}
			/>
		</>
	);
};

export default Cards;
