import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import styles from './Card.module.scss';
import { ReactComponent as FavouriteIcon } from '../../svg/favourite-icon.svg';
import { useDispatch, useSelector } from 'react-redux';
import {
	toggleProductInCart,
	toggleProductInFavourites,
} from '../../store/products/actionCreators';
import { setModalData, setModalIsOpen } from '../../store/modal/actionCreators';
import ViewSwitcherContext from '../../contexts/ViewSwitcherContext/ViewSwitcherContext';

const Card = ({ card: { title, thumbnail, price, color, id } }) => {
	const isFavourite = useSelector(
		store =>
			store.products.products.find(product => product.id === id).isFavourite
	);
	const isInCart = useSelector(
		store => store.products.products.find(product => product.id === id).isInCart
	);

	const { currentView } = useContext(ViewSwitcherContext);

	const dispatch = useDispatch();

	const addToFavouritesHandler = () => {
		dispatch(toggleProductInFavourites(id));
	};

	const addToCartHandler = () => {
		dispatch(setModalIsOpen(true));
		dispatch(
			setModalData({
				isProductInCart: isInCart,
				header: !isInCart
					? 'Додати товар до кошика?'
					: 'Видалити товар з кошика?',
				text: `Назва товару: ${title}`,
				handleSubmit: () => {
					dispatch(toggleProductInCart(id));
					dispatch(setModalIsOpen(false));
				},
			})
		);
	};

	return (
		<article
			className={`${styles.card} ${
				currentView.isCards ? styles.cardItemView : styles.tableItemView
			}`}
		>
			<button
				type="button"
				className={styles.addToFavouriteButton}
				onClick={addToFavouritesHandler}
			>
				<FavouriteIcon data-is-favourite={isFavourite} />
			</button>
			<div className={styles.imgContainer}>
				<img src={thumbnail} alt={title} />
			</div>
			<div className={styles.mainContent}>
				<h6 className={styles.title}>{title}</h6>
				<span className={styles.color}>
					<span>Колір:</span> {color}
				</span>
				<span className={styles.id}>Артикул: {id}</span>
			</div>
			<footer className={styles.footer}>
				<span className={styles.price}>{price}</span>
				<button className={styles.addToCartButton} onClick={addToCartHandler}>
					{isInCart ? 'Видалити з кошика' : 'Додати до кошика'}
				</button>
			</footer>
		</article>
	);
};

Card.propTypes = {
	card: PropTypes.shape({
		id: PropTypes.number.isRequired,
		title: PropTypes.string,
		price: PropTypes.number,
		thumbnail: PropTypes.string,
		color: PropTypes.string,
		isFavourite: PropTypes.bool,
		isInCart: PropTypes.bool,
	}),
};

Card.defaultProps = {
	card: {
		id: 0,
		title: '',
		price: 0,
		thumbnail: '',
		color: '',
		isFavourite: false,
		isInCart: false,
	},
};

export default Card;
