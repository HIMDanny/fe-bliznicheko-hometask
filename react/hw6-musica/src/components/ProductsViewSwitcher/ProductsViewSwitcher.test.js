import { render } from '@testing-library/react';
import ProductsViewSwitcher from './ProductsViewSwitcher';
import ViewSwitcherContextProvider from '../../contexts/ViewSwitcherContext/ViewSwitcherContextProvider';

const Component = () => (
	<ViewSwitcherContextProvider>
		<ProductsViewSwitcher />
	</ViewSwitcherContextProvider>
);

describe('ProductsViewSwitcher snapshot testing', () => {
	test('should ProductsViewSwitcher match snapshot', () => {
		const { asFragment } = render(<Component />);

		expect(asFragment()).toMatchSnapshot();
	});
});
