import React, { useContext } from 'react';
import styles from './ProductsViewSwitcher.module.scss';
import { ReactComponent as ViewListIcon } from '../../svg/view-list.svg';
import { ReactComponent as ViewModuleIcon } from '../../svg/view-module.svg';
import ViewSwitcherContext from '../../contexts/ViewSwitcherContext/ViewSwitcherContext';

const ProductsViewSwitcher = () => {
	const { currentView, setView } = useContext(ViewSwitcherContext);

	return (
		<ul className={styles.switcherWrapper}>
			<li className={styles.switcher}>
				<button
					className={`${styles.switcherButton} ${
						currentView.isTable ? styles.switcherButtonCurrent : null
					}`}
					onClick={() => setView('table')}
				>
					<ViewListIcon className={styles.viewIcon} />
				</button>
			</li>
			<li className={styles.switcher}>
				<button
					className={`${styles.switcherButton} ${
						currentView.isCards ? styles.switcherButtonCurrent : null
					}`}
					onClick={() => setView('cards')}
				>
					<ViewModuleIcon className={styles.viewIcon} />
				</button>
			</li>
		</ul>
	);
};

export default ProductsViewSwitcher;
