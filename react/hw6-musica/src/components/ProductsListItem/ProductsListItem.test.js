import { fireEvent, render, screen } from '@testing-library/react';
import ProductsListItem from './ProductsListItem';

describe('ProductsListItem snapshot testing', () => {
	test('should match snapshot', () => {
		const { asFragment } = render(
			<ProductsListItem
				product={{ id: 1, title: 'title', price: 2000, thumbnail: '' }}
				handleDelete={() => {}}
			/>
		);

		expect(asFragment()).toMatchSnapshot();
	});
});

describe('ProductsListItem delete button testing', () => {
	test('should call handleDelete function', () => {
		const handleDelete = jest.fn();

		render(
			<ProductsListItem
				product={{ id: 1, title: 'title', price: 2000, thumbnail: '' }}
				handleDelete={handleDelete}
			/>
		);

		const btn = screen.getByTestId('button');
		fireEvent.click(btn);

		expect(handleDelete).toHaveBeenCalled();
	});
});
