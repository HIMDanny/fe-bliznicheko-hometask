import CustomField from './CustomField';
import { render } from '@testing-library/react';
import { Formik, Form } from 'formik';

describe('CustomField snapshot testing', () => {
	test('should CustomFiel match snapshot', () => {
		const { asFragment } = render(
			<Formik
				initialValues={{
					name: '',
				}}
			>
				{() => (
					<Form>
						<CustomField className="test-class" label="Ім'я" name="name" />
					</Form>
				)}
			</Formik>
		);

		expect(asFragment()).toMatchSnapshot();
	});
});
