import orderReducer from './orderReducer';
import { createOrder } from './actionCreators';

describe('orderReducer testing', () => {
	test('should return the initial state', () => {
		expect(orderReducer(undefined, { type: undefined })).toEqual({
			orderInfo: {
				total: 0,
				products: [],
				userInfo: {},
			},
		});
	});

	test('should handle an order being created', () => {
		const previousState = {
			orderInfo: {
				total: 0,
				products: [],
				userInfo: {},
			},
		};

		const order = {
			total: 1,
			products: [
				{ id: 1, title: 'test', price: 0, isFavourite: false, isInCart: false },
			],
			userInfo: {
				name: 'name',
			},
		};

		expect(orderReducer(previousState, createOrder(order))).toEqual({
			orderInfo: {
				total: 1,
				products: [
					{
						id: 1,
						title: 'test',
						price: 0,
						isFavourite: false,
						isInCart: false,
					},
				],
				userInfo: {
					name: 'name',
				},
			},
		});
	});
});
