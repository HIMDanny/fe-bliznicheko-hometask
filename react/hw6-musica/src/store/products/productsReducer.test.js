import {
	clearCart,
	toggleProductInCart,
	toggleProductInFavourites,
} from './actionCreators';
import productsReducer from './productsReducer';

describe('productsReducer testing', () => {
	test('should return the initial state', () => {
		expect(productsReducer(undefined, { type: undefined })).toEqual({
			products: [],
			productsInCart: [],
			productsInFavourites: [],
		});
	});

	test('should handle getting products', () => {
		const products = [
			{ id: 1, title: 'test', price: 0, isFavourite: false, isInCart: false },
		];

		expect(
			productsReducer(undefined, { type: 'GET_PRODUCTS', payload: products })
		).toEqual({
			products: [
				{ id: 1, title: 'test', price: 0, isFavourite: false, isInCart: false },
			],
			productsInCart: [],
			productsInFavourites: [],
		});
	});

	test('should handle a product being added in favourites', () => {
		const previousState = {
			products: [
				{ id: 1, title: 'test', price: 0, isFavourite: false, isInCart: false },
			],
			productsInCart: [],
			productsInFavourites: [],
		};

		expect(
			productsReducer(previousState, toggleProductInFavourites(1))
		).toEqual({
			products: [
				{ id: 1, title: 'test', price: 0, isFavourite: true, isInCart: false },
			],
			productsInCart: [],
			productsInFavourites: [
				{ id: 1, title: 'test', price: 0, isFavourite: true, isInCart: false },
			],
		});
	});

	test('should handle a product being deleted from favourites', () => {
		const previousState = {
			products: [
				{ id: 1, title: 'test', price: 0, isFavourite: true, isInCart: false },
			],
			productsInCart: [],
			productsInFavourites: [],
		};

		expect(
			productsReducer(previousState, toggleProductInFavourites(1))
		).toEqual({
			products: [
				{ id: 1, title: 'test', price: 0, isFavourite: false, isInCart: false },
			],
			productsInCart: [],
			productsInFavourites: [],
		});
	});

	test('should handle a product being added in cart', () => {
		const previousState = {
			products: [
				{ id: 1, title: 'test', price: 0, isFavourite: false, isInCart: false },
			],
			productsInCart: [],
			productsInFavourites: [],
		};

		expect(productsReducer(previousState, toggleProductInCart(1))).toEqual({
			products: [
				{ id: 1, title: 'test', price: 0, isFavourite: false, isInCart: true },
			],
			productsInCart: [
				{ id: 1, title: 'test', price: 0, isFavourite: false, isInCart: true },
			],
			productsInFavourites: [],
		});
	});

	test('should handle a product being deleted from cart', () => {
		const previousState = {
			products: [
				{ id: 1, title: 'test', price: 0, isFavourite: false, isInCart: true },
			],
			productsInCart: [],
			productsInFavourites: [],
		};

		expect(productsReducer(previousState, toggleProductInCart(1))).toEqual({
			products: [
				{ id: 1, title: 'test', price: 0, isFavourite: false, isInCart: false },
			],
			productsInCart: [],
			productsInFavourites: [],
		});
	});

	test('should handle all products being deleted from cart', () => {
		const previousState = {
			products: [
				{
					id: 1,
					title: 'test1',
					price: 0,
					isFavourite: false,
					isInCart: true,
				},
				{
					id: 2,
					title: 'test2',
					price: 0,
					isFavourite: false,
					isInCart: true,
				},
			],
			productsInCart: [
				{
					id: 1,
					title: 'test1',
					price: 0,
					isFavourite: false,
					isInCart: true,
				},
				{
					id: 2,
					title: 'test2',
					price: 0,
					isFavourite: false,
					isInCart: true,
				},
			],
			productsInFavourites: [],
		};

		expect(productsReducer(previousState, clearCart())).toEqual({
			products: [
				{
					id: 1,
					title: 'test1',
					price: 0,
					isFavourite: false,
					isInCart: false,
				},
				{
					id: 2,
					title: 'test2',
					price: 0,
					isFavourite: false,
					isInCart: false,
				},
			],
			productsInCart: [],
			productsInFavourites: [],
		});
	});
});
