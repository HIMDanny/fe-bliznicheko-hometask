import {
	SET_MODAL_IS_OPEN,
	SET_MODAL_DATA,
} from "./actions";

export const setModalIsOpen = isOpen => ({ type: SET_MODAL_IS_OPEN, payload: isOpen });
export const setModalData = modalData => ({ type: SET_MODAL_DATA, payload: modalData });