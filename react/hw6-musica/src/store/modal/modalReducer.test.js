import modalReducer from './modalReducer';
import { setModalData, setModalIsOpen } from './actionCreators';

describe('modalReducer testing', () => {
	test('should return the initial state', () => {
		const initialState = {
			isOpen: false,
			modalData: {
				isProductInCart: false,
				header: '',
				text: '',
				handleSubmit: () => {},
			},
		};

		expect(modalReducer(initialState, { type: undefined })).toMatchObject(
			initialState
		);
	});

	test('should handle modal being opened', () => {
		const previousState = {
			isOpen: false,
		};

		const expected = {
			isOpen: true,
		};

		expect(modalReducer(previousState, setModalIsOpen(true))).toMatchObject(
			expected
		);
	});

	test('should handle modal being closed', () => {
		const previousState = {
			isOpen: true,
		};

		const expected = {
			isOpen: false,
		};

		expect(modalReducer(previousState, setModalIsOpen(false))).toMatchObject(
			expected
		);
	});

	test('should hadndle setting modal data', () => {
		const initialState = {
			isOpen: false,
			modalData: {
				isProductInCart: false,
				header: '',
				text: '',
				handleSubmit: () => {},
			},
		};

		const modalData = {
			isProductInCart: true,
			header: 'header',
			text: 'header',
			handleSubmit: jest.fn(),
		};

		const expected = {
			isOpen: false,
			modalData,
		};

		expect(modalReducer(initialState, setModalData(modalData))).toMatchObject(
			expected
		);
	});
});
