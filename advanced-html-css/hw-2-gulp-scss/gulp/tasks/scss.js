import dartSass from 'sass';
import gulpSass from 'gulp-sass';

import cleanCss from 'gulp-clean-css'; // Minify CSS
import autoPrefixer from 'gulp-autoprefixer'; // Prefix CSS
import groupCssMediaQueries from 'gulp-group-css-media-queries'; // CSS postprocessing: group media queries
import purgeCss from 'gulp-purgecss'; // Delete unused CSS

const sass = gulpSass(dartSass);

export const scss = () => {
	return app.gulp.src(app.path.src.scss, { sourcemaps: app.isDev })
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: "SCSS",
				message: "Error <%= error.message %>"
			})
		))
		.pipe(sass({
			outputStyle: 'expanded',
		}))
		.pipe(groupCssMediaQueries())
		.pipe(autoPrefixer({
			grid: true,
			overrideBrowserslist: ['last 2 versions'],
			cascade: true,
		}))
		.pipe(app.plugins.if(app.isBuild, purgeCss({
			content: ['src/**/*.html']
		})))
		// In case when not minified version is needed
		.pipe(app.plugins.if(app.isDev, app.gulp.dest(app.path.build.css)))
		.pipe(cleanCss())
		.pipe(app.plugins.rename({
			basename: 'styles',
			extname: '.min.css',
		}))
		.pipe(app.gulp.dest(app.path.build.css))
		.pipe(app.plugins.browserSync.stream());
}