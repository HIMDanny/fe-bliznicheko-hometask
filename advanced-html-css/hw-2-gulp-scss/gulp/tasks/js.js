import terser from "gulp-terser";

export const js = () => {
	return app.gulp.src(app.path.src.js, { sourcemaps: app.isDev })
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: "JS",
				message: "Error <%= error.message %>"
			})
		))
		.pipe(app.plugins.concat('scripts.min.js'))
		.pipe(terser())
		// If terser needed only in Build, uncomment line below and comment line above and vice versa
		// .pipe(app.plugins.if(app.isBuild, terser())) 
		.pipe(app.gulp.dest(app.path.build.js))
		.pipe(app.plugins.browserSync.stream());
}