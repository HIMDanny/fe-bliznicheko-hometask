import fileInclude from "gulp-file-include"; // Includes files
import browserSync from "browser-sync"; // Local server
import rename from 'gulp-rename'; // Rename files
import concat from 'gulp-concat'; // Concat files
import plumber from "gulp-plumber"; // Error processing
import notify from "gulp-notify" // Messages (hints)
import gulpIf from "gulp-if"; // If statement


export const plugins = {
	fileInclude,
	browserSync,
	rename,
	concat,
	plumber,
	notify,
	if: gulpIf,
}