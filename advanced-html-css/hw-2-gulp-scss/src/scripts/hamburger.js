const hamburgerMenuHandler = () => {
	const hamburger = document.querySelector('.nav__hamburger');
	const menu = document.querySelector('.nav__list');

	hamburger.addEventListener('click', (e) => {
		e.currentTarget.classList.toggle('show');

		if (hamburger.classList.contains('show')) {
			menu.classList.add('show');
		}
		else {
			menu.classList.remove('show');
		}
	});

	document.addEventListener('click', (e) => {
		if (!e.target.matches('.nav__hamburger') && !e.target.matches('.nav__hamburger *')) {
			hamburger.classList.remove('show');
			menu.classList.remove('show');
		}
	});
}

hamburgerMenuHandler();